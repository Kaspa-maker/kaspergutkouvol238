﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MouseMovement : MonoBehaviour
{
    float mouseLocation;
    Vector2 position;
    Rigidbody2D rb;
    public float force = 10f;
    // Start is called before the first frame update
    void Start()
    {
        OnMouseDrag();
    }

    // Update is called once per frame
    void Update()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnMouseDrag()
    {
        //mouseLocation = Camera.main.WorldToScreenPoint(gameObject.transform.position).x;
        position = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, 0));

        if (Input.GetMouseButton(0))
        {
            if (GameObject.FindGameObjectWithTag("PickUp"))
            {
                transform.position = new Vector2(position.x, 0);
            }
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.collider.gameObject)
        {
            //transform.position = new Vector2(transform.position.x, transform.position.y);
            rb.AddForce(transform.forward * force);
        }
    }
}
