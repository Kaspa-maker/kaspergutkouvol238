﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFeature : MonoBehaviour
{
    public GameObject Tower;
    public GameObject bulletPrefab;
    public GameObject firePoint;
    public float speed = 8f;
    public float lifeTime = 2f;
    public float rotateTimer;
    public float fireTimer;
    public float shootTimer;
    private float shotCount = 12;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rotateTimer += Time.deltaTime;
        fireTimer += Time.deltaTime;

        if (rotateTimer > 0.5f)
        {
            Tower.transform.Rotate(0, 0, 2);
        }

        if (fireTimer > 2)
        {
            
            GameObject bullet = Instantiate(bulletPrefab);
            bullet.transform.position = firePoint.transform.position;
            fireTimer = 0;
        }
    }

}
